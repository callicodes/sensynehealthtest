import React, { Component, useState } from "react";
import Button from "@material-ui/core/Button";

// Alternative Bugfix below - functional component version

/* 

function QuestionOne() {
  const [label] = useState("I've been clicked: ");
  const [count, setCount] = useState(0);

  function increment() {
    setCount((prevCount) => prevCount + 1);
  }

 return (
    <div style={{ marginTop: 48 }}>
      <Button variant="contained" onClick={increment}>
        {label} {count} times
      </Button>
    </div>
  );
} 


*/

class QuestionOne extends Component {
  constructor(props) {
    super(props);
    this.state = {
      label: "I've been clicked: ",
      counter: 0,
    };
  }

  // Bugfix: method needed to be moved outside the render function and this.handleOnClick

  handleOnClick = () => {
    this.setState({
      counter: this.state.counter + 1,
    });
  };
  render() {
    return (
      <div style={{ marginTop: 48 }}>
        <Button variant="contained" onClick={this.handleOnClick}>
          {this.state.label} {this.state.counter} times
        </Button>
      </div>
    );
  }
}

export default QuestionOne;
